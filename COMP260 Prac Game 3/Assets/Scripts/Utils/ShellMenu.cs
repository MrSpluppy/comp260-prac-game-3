﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ShellMenu : MonoBehaviour {
    
    public GameObject shellPanel;
    public GameObject optionsPanel;
    public Dropdown qualityDropdown;
    public Dropdown resolutionDropdown;
    public Toggle fullscreenToggle;
    public Slider volumeSlider;
    public Slider musicSlider;
    public bool ignoreListenerVolume;


    private bool paused = true;

    void Start()
    {
        // options panel is initally hidden
        optionsPanel.SetActive(false);
        SetPaused(paused);

        // populate the list of video quality levels
        qualityDropdown.ClearOptions();
        List<string> names = new List<string>();
        for (int i = 0; i < QualitySettings.names.Length; i++)
        {
            names.Add(QualitySettings.names[i]);
        }
        qualityDropdown.AddOptions(names);

        // populate the list of available resolutions
        resolutionDropdown.ClearOptions();
        List<string> resolutions = new List<string>();
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            resolutions.Add(Screen.resolutions[i].ToString());
        }
        resolutionDropdown.AddOptions(resolutions);

        // restore the saved audio volume
        if (PlayerPrefs.HasKey("AudioVolume"))
        {
            AudioListener.volume =
                PlayerPrefs.GetFloat("AudioVolume");
        }
        else
        {
            // first time the game is run, use the default value
            AudioListener.volume = 1;
        }

        // restore the saved music volume
        if (PlayerPrefs.HasKey("MusicVolume"))
        {
            AudioListener.volume =
                PlayerPrefs.GetFloat("MusicVolume");
        }
        else
        {
            // first time the game is run, use the default value
            AudioListener.volume = 1;
        }
    }


    public void OnPressedOptions()
    {
        // show the options panel & hide the shell panel
        shellPanel.SetActive(false);
        optionsPanel.SetActive(true);

        // select the current quality value
        qualityDropdown.value = QualitySettings.GetQualityLevel();
       
        // select the current resolution
        int currentResolution = 0;
        for (int i = 0; i < Screen.resolutions.Length; i++)
        {
            if (Screen.resolutions[i].width == Screen.width &&
                Screen.resolutions[i].height == Screen.height)
            {
                currentResolution = i;
                break;
            }
        }
        resolutionDropdown.value = currentResolution;

        // set the fullscreen toggle
        fullscreenToggle.isOn = Screen.fullScreen;

        // set the volume slider
        volumeSlider.value = AudioListener.volume;

        // set the music slider
        musicSlider.value = AudioListener.volume;
    }

    public void OnPressedCancel()
    {
        // return to the shell menu
        shellPanel.SetActive(true);
        optionsPanel.SetActive(false);
    }

    public void OnPressedApply()
    {
        // apply the changes
        QualitySettings.SetQualityLevel(qualityDropdown.value);
        Resolution res =
            Screen.resolutions[resolutionDropdown.value];
        Screen.SetResolution(res.width, res.height,
                             fullscreenToggle.isOn);
        AudioListener.volume = volumeSlider.value;
        PlayerPrefs.SetFloat("AudioVolume", AudioListener.volume);

        AudioListener.volume = musicSlider.value;
        PlayerPrefs.SetFloat("MusicVolume", AudioListener.volume);

        // return to the shell menu
        shellPanel.SetActive(true);
        optionsPanel.SetActive(false);


    }


    void Update()
    {
        if (!paused && Input.GetKeyDown(KeyCode.Escape))
        {
            SetPaused(true);
        }
    }

    private void SetPaused(bool p)
    {
        paused = p;
        shellPanel.SetActive(paused);
        Time.timeScale = paused ? 0 : 1;
    }

    public void OnPressedPlay()
    {
        // resume the game
        SetPaused(false);
    }

    public void OnPressedQuit()
    {
        // quit the game
        Application.Quit();
    }
}
