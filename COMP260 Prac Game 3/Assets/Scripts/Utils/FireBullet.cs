﻿using UnityEngine;
using System.Collections;

public class FireBullet : MonoBehaviour {

    public BulletMove bulletPrefab;
    public float reloadTime;
    private float reload = 10;

    void Update()
    {

        // do not run if the game is paused
        if (Time.timeScale == 0)
        {
            return;
        }

        reload += Time.deltaTime;
        // when the button is pushed, fire a bullet
        if (Input.GetButtonDown("Fire1") && reload >= reloadTime)
        {

            BulletMove bullet = Instantiate(bulletPrefab);
            // the bullet starts at the player's position
            bullet.transform.position = transform.position;

            // create a ray towards the mouse location
            Ray ray =
                Camera.main.ScreenPointToRay(Input.mousePosition);
            bullet.direction = ray.direction;

            reload = 0;
        }
    }
}
